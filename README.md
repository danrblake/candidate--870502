<h1>Technical Assessment - Read Me</h1>
<h2>Contents</h2>
<ul>
<li>Pre-Requisites/Setup</li>
<li>The Task</li>
<li>How I approached this Task</li>
<li>Scenarios and Tags</li>
<li>How to run the tests</li>
</ul>
<h2>Pre-Requisites/Setup</h2>
<p>In order to use this project you will need the following technologies installed:
<ol>
<li>Maven</li>
<li>Git</li>
<li>Java 8+</li>
<li>Webdrivers (Chrome, Edge, Firefox etc)</li>
</ol>
<p>The easiest way to ensure all of these technologies are installed and environment variables set to the correct locations
is using a Package Manager</p>
<h3>Windows</h3>
<p>For Windows this would be Chocolatey. Installation instructions available on </p>
<pre><a href="https://chocolatey.org/install">Chocolatey Website</a></pre>
<p>Once chocolatey is installed simply install the technologies above using "choco install ...." e.g. "choco install chromedriver". 
<h3>Linux</h3>
<p>A Package Manager for Linux is Homebrew. Installation instructions are available on</p>
<pre><a href="https://brew.sh" title="Homebrew Website">Homebrew Website</a></pre>
<p>Once Homebrew is installed simply install the technologies using "brew cask install...." e.g. "brew cask install chromedriver"</p>
<h3>Mac OS</h3>
<p>For Mac Users use npm. Installation instructions are available on</p>
<pre><a href="https://www.npmjs.com/get-npm">npm Website</a></pre>
<p>Once npm is installed simply install the technologies using "npm install...." e.g. "npm install chromedriver"</p>
<h3>Alternatively</h3>If you don't mind setting up the drivers by yourself you will need to download the executables for the drivers and you will need to provide the path to these drivers inside the serenity.properties file. <br><br></br>For example: </p>
<pre>webdriver.chrome.driver=insert_path_to_chromedriver.exe</pre>
<h2>The Task</h2>
<p>For this assignment I was asked to complete the following brief:</p>
<pre>     Application under test: Check UK visa website; URL: https://www.gov.uk/check-uk-visa/y
     Feature – Confirm whether a visa is required to visit the UK 
     <br>     Scenarios:
     <br>     Given I provide a nationality of Japan
     And I select the reason “Study”
     And I state I am intending to stay for more than 6 months
     When I submit the form
     Then I will be informed “I need a visa to study in the UK”
     <br>     Given I provide a nationality of Japan
     And I select the reason “Tourism”
     When I submit the form
     Then I will be informed “I won’t need a visa to study in the UK”
     <br>     Given I provide a nationality of Russia
     And I select the reason “Tourism”
     And I state I am not travelling or visiting a partner or family
     When I submit the form
     Then I will be informed “I need a visa to come to the UK”
     <br>
     Application under test: Postcodes Api
     Feature –  Query a postcode and receive a 200 response
     <br>     Scenarios:
     When I send a get request to api.postcodes.io/postcodes/SW1P4JA
     Then I get a 200 response</pre>
<h2>How I approached the task</h2>
<p>In order to complete this assignment I decided to follow these steps:</p>
<ol>
<li>Create Maven Project and link to GitLab</li>
<li>Set up POM.xml, including Serenity Reporting Configuration</li>
<li>Set up initial runner</li>
<li>Set up feature for Visa Checks</li>
<li>Set up step definitions and Page Objects</li>
<li>Complete Visa Check Scenarios and run tests</li>
<li>Set up feature for API Test</li>
<li>Set up step definitons for API Test</li>
<li>Complete API Test Scenario</li>
<li>Ensure all tests work when run via Runner or via Command Line</li>
<li>Improve the speed of Text Execution with the implementation of Parallel Test Running</li>
</ol>
<p>Through best practices I was able to heavily reduce code required for these tests using the "Extensibility" principle which allowed me to not write any new code for the second UI Scenario.
Also the implementation of the Parallel Test Execution has reduced the total time of execution from roughly 45 seconds to 27 seconds (almost half the time!)</p>
<h2>Scenarios and Tags</h2>
<p>Each of the scenarios in the assignment have a separate tag. In order of the specification provided above they are:</p>
<ol><li>UI Scenarios<ul><li>@JapaneseStudentForMoreThan6Months</li>
                    <li>@JapaneseTourism</li>
                    <li>@RussianTourismNoFamily</li>
                    </ul>
<li>Api Scenarios<ul><li>@PostCodesAPI</li></ul></ol>

In order to run all the UI Scenarios you can use **@VisaChecks** and to run all tests use **@AllTests**

<h2>How to Run the Tests</h2>

<p>In the final set up where Parallel Test Execution has been implemented each Runner currently has a single tag in each for the four tests created. Also the browser that will be run is chrome as specified in the serenity.properties file. If you would like to run this pack provided then run the following from the command line</p>
<pre>mvn clean verify</pre>
<p>If you would like to change the browser that will run the tests then you will need to adjust the webdriver.driver value in the serenity.properties file.
<br>
<br>The only drawback here is that the Edge Browser does not support parallel test execution
<br><br>
If you would like to run a singular tag then you will need to adjust the POM.xml file line 102 (currently **/TestSuite*.java) to the runner of your choice e.g. **/TestSuite1.java as well as providing the tag you wish to run in the specified runner.</p>