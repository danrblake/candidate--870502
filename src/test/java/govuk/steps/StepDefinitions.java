package govuk.steps;

import govuk.Pages.Nationality;
import govuk.Pages.ReasonForVisa;
import govuk.Pages.LengthOfStay;
import govuk.Pages.DoINeedVisa;
import govuk.Pages.TravellingWithOrVisiting;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {

    private Nationality initialCheckVisasPage;
    private ReasonForVisa reasonForVisaPage;
    private LengthOfStay lengthOfStayPage;
    private DoINeedVisa doINeedVisaPage;
    private TravellingWithOrVisiting travellingWithOrVisitingPage;

    @Given("I am on the Check UK Visa Website")
    public void i_am_on_the_Check_UK_Visa_Website() {
        initialCheckVisasPage.navigateToVisaCheckPage();
    }

    @When("I provide a nationality of {string}")
    public void iProvideANationalityOfJapan(String nationality) {
        initialCheckVisasPage.selectNationality(nationality);
        initialCheckVisasPage.clickNextStep();
    }

    @And("I state I am intending to stay for {string} than 6 months")
    public void iStateIAmIntendingToStayForMoreThanMonths(String moreOrLess) {
        lengthOfStayPage.selectStayLength(moreOrLess);
        lengthOfStayPage.clickNextStep();
    }

    @And("I select the reason {string}")
    public void iSelectTheReasonStudy(String reason) {
        reasonForVisaPage.selectReason(reason);
        reasonForVisaPage.clickNextStep();
    }

    @And("I submit the form")
    public void iSubmitTheForm() {
        lengthOfStayPage.clickNextStep();
    }

    @Then("I will be informed {string}")
    public void iWillBeInformed(String expectedResponse) {
        doINeedVisaPage.validateVisaOutcome(expectedResponse);
    }

    @And("I state I am not travelling or visiting a partner or family")
    public void iStateIAmNotTravellingOrVisitingAPartnerOrFamily() {
        travellingWithOrVisitingPage.selectWithout();
        travellingWithOrVisitingPage.clickNextStep();
    }
}
