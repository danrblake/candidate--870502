package govuk.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.junit.Assert;

public class APIStepDefinitions {

    private int responseCode;

    @When("I send a request with Postcode {string} to Postcodes IO")
    public void iSendARequestWithPostcodeToPostcodesIO(String queryPostcode) {
        responseCode = SerenityRest.get("https://api.postcodes.io/postcodes/" + queryPostcode).statusCode();
    }

    @Then("I get a response {int} code")
    public void iGetAResponseCode(int expectCode) {
            Assert.assertEquals(responseCode, expectCode);
        }
}
