package govuk.Runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
        tags = {"@JapaneseTourism"},
        plugin = {"pretty"},
        features="src/test/resources/features",
        glue = "govuk.steps")

public class TestSuite2 {
}
