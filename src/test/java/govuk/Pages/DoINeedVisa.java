package govuk.Pages;

import org.assertj.core.api.Fail;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.NoSuchElementException;

public class DoINeedVisa extends CommonPageObjects {

    public void validateVisaOutcome(String expectedResponse) {
        try {
            Assert.assertTrue(isElementVisible(By.xpath("//*[text()='" + expectedResponse + "']")));
        } catch (NoSuchElementException e) {
            Fail.fail("Result Response was not seen, test failed");
        }
    }
}
