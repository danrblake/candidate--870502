package govuk.Pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class TravellingWithOrVisiting extends CommonPageObjects {
    @FindBy(xpath = "//label[@for='response-0']")
    private WebElementFacade yesRadioButton;

    @FindBy(xpath = "//label[@for='response-1']")
    private WebElementFacade noRadioButton;


    public void selectWithout() {
        clickOn(noRadioButton);
    }
}
