package govuk.Pages;

import net.serenitybdd.core.annotations.findby.By;

public class ReasonForVisa extends CommonPageObjects {


    public void selectReason(String reason) {
        clickOn(getDriver().findElement(By.xpath("//input[@value='" + reason.toLowerCase() + "']")));
    }
}
