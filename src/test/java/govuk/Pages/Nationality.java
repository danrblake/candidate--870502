package govuk.Pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class Nationality extends CommonPageObjects {
    private String visaUrl = "https://www.gov.uk/check-uk-visa/y";

    @FindBy(id = "response")
    private WebElementFacade nationalityDropDown;

    @FindBy(xpath = "//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")
    private WebElementFacade nextStepButton;

    public void navigateToVisaCheckPage() {
        getDriver().navigate().to(visaUrl);
    }

    public void selectNationality(String nationality) {
        clickOn(nationalityDropDown);
        clickOn(getDriver().findElement(By.xpath("//option[text()='" + nationality + "']")));
    }

}
