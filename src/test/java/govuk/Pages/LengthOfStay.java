package govuk.Pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.WebElement;

public class LengthOfStay extends CommonPageObjects {

    @FindBy(xpath = "//label[@for='response-0']")
    private WebElementFacade lessThanSixMonths;

    @FindBy(xpath = "//label[@for='response-1']")
    private WebElement moreThanSixMonths;

    public void selectStayLength(String moreOrLess) {
        switch (moreOrLess) {
            case "more":
                clickOn(moreThanSixMonths);
                break;
            case "less":
                clickOn(lessThanSixMonths);
                break;
            default:
                break;
        }
    }
}
