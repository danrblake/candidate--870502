package govuk.Pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class CommonPageObjects extends PageObject {
    @FindBy(xpath = "//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")
    private WebElementFacade nextStepButton;

    private JavascriptExecutor js = (JavascriptExecutor) getDriver();

    public void clickNextStep() {
        clickOn(nextStepButton);
    }
    public void executeJavascript(String javaScript, WebElement element) {
        js.executeScript(javaScript, element);
    }
}
