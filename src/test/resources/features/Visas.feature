@VisaChecks @AllTests
Feature: GOV UK Visas
  @JapaneseStudentForMoreThan6Months
  Scenario: I am a japanese national who wants to come to the UK for more than 6 months to study
    Given I am on the Check UK Visa Website
    When I provide a nationality of "Japan"
    And I select the reason "Study"
    And I state I am intending to stay for "more" than 6 months
    Then I will be informed "You’ll need a visa to study in the UK"
    
  @JapaneseTourism  
  Scenario: I am a Japanese national who wants to come to the UK for Tourism
    Given I am on the Check UK Visa Website
    When I provide a nationality of "Japan"
    And I select the reason "Tourism"
    Then I will be informed "You won’t need a visa to come to the UK"
    
  @RussianTourismNoFamily
  Scenario: I am a Russian national who wants to come to the UK for Tourism and I am not travelling with or visiting a partner or family
    Given I am on the Check UK Visa Website
    When I provide a nationality of "Russia"
    And I select the reason "Tourism"
    And I state I am not travelling or visiting a partner or family
    Then I will be informed "You’ll need a visa to come to the UK"
