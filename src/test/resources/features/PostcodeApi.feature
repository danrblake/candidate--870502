@PostCodesAPI @AllTests
  Feature: Querying PostCodes IO
    Scenario: When I query the Post Code IO with address SW1 P4JA I get a 200 response
      When I send a request with Postcode "SW1P4JA" to Postcodes IO
      Then I get a response 200 code